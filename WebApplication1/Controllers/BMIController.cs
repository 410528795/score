﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.ViewModel;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (ModelState.IsValid) {
                if (data.Height < 50 || data.Height > 200)
                {
                    ViewBag.HeightError = "身高請輸入50~200的數值";
                }
                if (data.Weight < 30 || data.Weight > 150)
                {
                    ViewBag.WeightError = "體重請輸入50~200的數值";
                }
                float m_height = data.Height / 100;
                float BMI = data.Weight / (m_height * m_height);
                var level = "";
            }
            return View(data);
        }
        
        
    }

}
