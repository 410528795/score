﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class abcdController : Controller
    {
        // GET: abcd
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(float score)
        {
            
            
            string code= "";
            if (score >= 0 && score < 20)
            {
                code = "E";
            }
            else if (20<= score && score <= 39)
            {
                code = "D";
            }
            else if (40 <= score && score <= 59)
            {
                code = "C";
            }
            else if (60 <= score && score <= 79)
            {
                code = "B";
            }
            else if (80 <= score && score <= 100)
            {
                code = "A";
            }
            
            ViewBag.score = score;
            ViewBag.code = code;
            return View();
        }

    }
}