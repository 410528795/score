﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {
        [Display(Name = "體重")]
        
        public float Weight { get; set; }
        
        public float Height { get; set; }
        public float BMI { get; set; }
        public string Level { get; set; }

    }
}